import datetime
import os


class ImageFile():
    def __init__(self, file_name, dir_name, time=None):
        self.file_name = file_name
        self.dir_name = dir_name
        self.time = None

    def getNameAndTime(self):
        return self.file_name + ":" + str(self.time)


def get_files_from_dir(dir_name, except_dir, file_extension):
    files = []
    for file in os.scandir(dir_name):
        if file.name not in except_dir:
            f = os.path.join(os.path.abspath(dir_name), file.name)
            fileObject = ImageFile(file.name, f)
            if (file.is_dir()):
                try:
                    files += get_files_from_dir(f, except_dir, file_extension)
                except Exception:
                    pass
                    # print("Was not able check " + f)
            else:
                if file_extension is None:
                    files.append(fileObject)
                elif file.name.lower().endswith(file_extension):
                    files.append(fileObject)
    return files


def return_file_list_with_doubles(files_list):
    file_names_and_date = []
    result_list = []
    for file in files_list:
        if len(file.dir_name) < 255:
            file.time = os.path.getmtime(file.dir_name)
            file_names_and_date.append(file.getNameAndTime())

    for file in files_list:
        if file_names_and_date.count(file.getNameAndTime()) > 1:
            result_list.append(file)
    return result_list


if __name__ == '__main__':
    root_path_for_searching = "C:\!SR"
    except_directories = ['Project', 'Projects']
    file_extension = ".zip"

    start_date = datetime.datetime.now()
    print("Started working in " + str(datetime.datetime.now()))

    files_list = get_files_from_dir(root_path_for_searching, except_directories, file_extension)
    print("Total files: {}".format(len(files_list)))

    tmp_names_doubles = dict()
    for file in files_list:
        if file.file_name not in tmp_names_doubles.keys():
            tmp_names_doubles[file.file_name] = [file]
        else:
            tmp_names_doubles[file.file_name].append(file)

    name_doubles = dict()
    for key in tmp_names_doubles.keys():
        if len(tmp_names_doubles[key]) > 1:
            files_list = tmp_names_doubles[key]
            res_file_list = return_file_list_with_doubles(files_list)
            name_doubles[key] = res_file_list

    result_doubles = dict()
    for key in name_doubles.keys():
        if len(name_doubles[key]) > 0:
            result_doubles[key] = name_doubles[key]

    end_date = datetime.datetime.now()
    print((end_date - start_date))

    print("Total doubles: {}".format(len(result_doubles)))
    with open("result.txt", 'w', encoding='utf-8') as f:
        for result in result_doubles.keys():
            string = result + '\n'
            for value in result_doubles[result]:
                string += "   " + value.dir_name + '\n'
            string += "\n"
            f.write(string)
